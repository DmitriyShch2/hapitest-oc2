'use strict'

const Hapi = require('hapi');

const server = Hapi.server({
    host: 'localhost',
    port: 8080
});

server.route({
    method: 'GET',
    path: '/hello',
    handler: function(request, h) {
        return 'Hello World!'
    }
});

server.route({
    method: 'GET',
    path: '/',
    handler: function(request, h) {
        return 'Hello my Friend!'
    }
});

server.route({
    method: 'GET',
    path: '/hello/{name}',
    handler: function(request, h) {
        return 'Hello World with param1 ' + request.params.name;
    }
});

async function start() {
    try {
        await server.start();
    } catch(err) {
        console.log(err);
        process.exit(1);
    }

    console.log('Server running at: ', server.info.uri);
}

start();